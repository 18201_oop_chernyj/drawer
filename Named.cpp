//
// Created by n.chernyj on 27.11.2019.
//

#include "Named.h"

std::map<std::string, int> Named::shape_counter;

std::string Named::generateName(const std::string &shape_name) {
    shape_counter.insert(std::pair<std::string, int>(shape_name, 0));
    std::string new_name;
    new_name = shape_name + '.' + std::to_string(shape_counter[shape_name]);
    shape_counter[shape_name]++;
    return new_name;
}

std::string Named::getName() const{
    return name;
}

void Named::setName(const std::string new_name) {
    name = new_name;
}