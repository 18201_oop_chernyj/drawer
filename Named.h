//
// Created by n.chernyj on 27.11.2019.
//

#ifndef DRAWER_NAMED_H
#define DRAWER_NAMED_H

#include <string>
#include <map>



class Named{
    static std::map<std::string, int> shape_counter;
    std::string name;

public:
    Named(std::string new_name) {name = new_name;}
    Named(){};
    static std::string generateName(const std::string& shape_name);
    std::string getName() const;
    void setName(const std::string new_name);
};
#endif //DRAWER_NAMED_H
