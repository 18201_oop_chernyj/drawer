//
// Created by admin on 20.11.2019.
//

#include "Shape.h"
//using namespace shape;

void Shape::draw(image_drawer& drawer){
    std::vector<segment> segments;
    segments = getSegments();
    drawer.pen_color(get_red(), get_green(), get_blue());

    for (auto& segm : segments){
        drawer.line_segment(segm.point_1.first, segm.point_1.second, segm.point_2.first, segm.point_2.second);
    }
}


Circle::Circle(std::pair<double, double> center, double rad)
{
    coordinates = center;
    radius = rad;
    setName(generateName("Circle"));
}
Circle::Circle(double center_x, double center_y, double rad)
{
    coordinates = std::pair<double, double>(center_x, center_y);
    radius = rad;
    setName(generateName("Circle"));
    }
Circle::Circle(double rad)
{
    coordinates = std::pair<double, double>(0,0);
    radius = rad;
    setName(generateName("Circle"));
    }

double Circle::square() const
{
    return radius * radius * PI;
}

std::vector<segment> Circle::getSegments() const
{
    std::vector<segment> segments;
    segment new_segment;
    new_segment.point_2 = std::pair<int,int>(coordinates.first, coordinates.second + radius);
    for (double i = 0.0; i < 2 * PI; i += 1 / radius)
    {
        new_segment.point_1 = new_segment.point_2;
        new_segment.point_2 = std::pair<double, double>(coordinates.first + radius * sin(i), coordinates.second + radius * cos(i));
        segments.push_back(new_segment);
    }

    return segments;
}

Rectangle::Rectangle(std::pair<double, double> center, std::pair<double, double> dims)
{
    coordinates = center;
    dimensions = dims;
    setName(generateName("Rectangle"));
}

Rectangle::Rectangle(std::pair<double, double> dims)
{
    coordinates = std::pair<double, double>(0,0);
    dimensions = dims;
    setName(generateName("Rectangle"));
}

Rectangle::Rectangle(double center_x, double center_y, double dimensions_x, double dimensions_y)
{
    coordinates = std::pair<double, double>(center_x, center_y);
    dimensions = std::pair<double, double>(dimensions_x, dimensions_y);
    setName(generateName("Rectangle"));
}

Rectangle::Rectangle(double dimensions_x, double dimensions_y)
{
    coordinates = std::pair<double, double>(0,0);
    dimensions = std::pair<double, double>(dimensions_x, dimensions_y);
    setName(generateName("Rectangle"));
}

double Rectangle::square() const
{
    return dimensions.first * dimensions.second;
}

std::vector<segment> Rectangle::getSegments() const
{
    std::vector<segment> segments;
    segment new_segment;
    new_segment.point_1 = std::pair<double , double>(coordinates.first - dimensions.first/2,coordinates.second - dimensions.second/2);
    new_segment.point_2 = std::pair<double , double>(coordinates.first + dimensions.first/2,coordinates.second - dimensions.second/2);
    segments.push_back(new_segment);

    new_segment.point_1 = std::pair<double , double>(coordinates.first + dimensions.first/2,coordinates.second - dimensions.second/2);
    new_segment.point_2 = std::pair<double , double>(coordinates.first + dimensions.first/2,coordinates.second + dimensions.second/2);
    segments.push_back(new_segment);

    new_segment.point_1 = std::pair<double , double>(coordinates.first + dimensions.first/2,coordinates.second + dimensions.second/2);
    new_segment.point_2 = std::pair<double , double>(coordinates.first - dimensions.first/2,coordinates.second + dimensions.second/2);
    segments.push_back(new_segment);

    new_segment.point_1 = std::pair<double , double>(coordinates.first - dimensions.first/2,coordinates.second + dimensions.second/2);
    new_segment.point_2 = std::pair<double , double>(coordinates.first - dimensions.first/2,coordinates.second - dimensions.second/2);
    segments.push_back(new_segment);

    return segments;
}

Triangle::Triangle(std::pair<double, double> coord_0, std::pair<double, double> coord_1,
                   std::pair<double, double> coord_2)
{
    coordinates = coord_0;
    point_1 = coord_1;
    point_2 = coord_2;
    setName(generateName("Triangle"));
}
Triangle::Triangle(double point0_x, double point0_y, double point1_x, double point1_y, double point2_x, double point2_y)
{
    coordinates = std::pair<double, double>(point0_x, point0_y);
    point_1 = std::pair<double, double>(point1_x, point1_y);
    point_2 = std::pair<double, double>(point2_x, point2_y);
    setName(generateName("Triangle"));
}
Triangle::Triangle(std::pair<double, double> coord_1, std::pair<double, double> coord_2)
{
    coordinates = std::pair<double, double>(0,0);
    point_1 = coord_1;
    point_2 = coord_2;
    setName(generateName("Triangle"));
}

double Triangle::square() const
{
    return std::fabs(point_1.first * point_2.second - point_1.second * point_2.first )/2;
}

std::vector<segment> Triangle::getSegments() const
{
    std::vector<segment> segments;
    segment new_segment;

    new_segment.point_1 = coordinates;
    new_segment.point_2 = std::pair<double, double> (coordinates.first + point_1.first, coordinates.second + point_1.second);
    segments.push_back(new_segment);

    new_segment.point_1 = new_segment.point_2;
    new_segment.point_2  = std::pair<double, double> (coordinates.first + point_2.first, coordinates.second + point_2.second);;
    segments.push_back(new_segment);

    new_segment.point_1 = new_segment.point_2;
    new_segment.point_2 = coordinates;
    segments.push_back(new_segment);

    return segments;
}


