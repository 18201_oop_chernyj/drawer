#define CATCH_CONFIG_MAIN

#include "Shape.h"
#include "ShapeFunctions.hpp"
#include "catch.hpp"

//using namespace shape;

TEST_CASE("Shape","square + totalArea"){
    Rectangle rectangle(std::pair<double, double>(10, 10));
    Circle circle(1);
    Triangle triangle(0, 0, 0, 10, 10, 0);
    REQUIRE(rectangle.square() == 100);
    REQUIRE(fabs(circle.square() - 3.14) < 0.01);
    REQUIRE(triangle.square() == 50);
    REQUIRE(fabs(totalArea(triangle, circle, rectangle) - 153.14) < 0.01);
}



TEST_CASE("Names","printNames"){
    std::stringstream stream_out;
    Circle circle_1(10);
    Rectangle rectangle_1(20, 10), rectangle_2(20, 10);
    Named named("something");
    Triangle triangle(std::pair<double, double>(10, 10), std::pair<double, double>(0, 10));
    printNames(stream_out, circle_1, rectangle_1, rectangle_2, named, triangle);
    std::cout << stream_out.str();
    REQUIRE(stream_out.str().compare("Circle.1 Rectangle.1 Rectangle.2 something Triangle.1 \n") == 0);
}

TEST_CASE("Draw") {
    bitmap_image img(1000, 1000);
    img.set_all_channels(255, 255, 255);
    image_drawer drawer(img);
    drawer.pen_width(1);
    Circle circle(500, 500, 100);
    circle.set_color("Red");
    circle.draw(drawer);
    Rectangle rectangle(400, 400, 100, 100);
    rectangle.set_color(0, 255, 0);
    rectangle.draw(drawer);
    Triangle triangle(300, 300, 0, 50, 50, 0);
    triangle.draw(drawer);
    img.save_image("image_test.bmp");
}
