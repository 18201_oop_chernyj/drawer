#include <iostream>
#include "Shape.h"
#include "ShapeFunctions.hpp"

int main(int argc, char* argv[]) {
    if (argc > 1) {
        std::ifstream file(argv[1]);
        if (argc == 2)
            drawInput(file, "drawer.bmp");
        else
            drawInput(file, argv[2]);
    }
    else
        drawInput(std::cin, "drawer.bmp");
}
