//
// Created by n.chernyj on 27.11.2019.
//

#include "Color.h"

Color::Color()
{
    red = 0;
    green = 0;
    blue = 0;
}

void Color::set_color(int c_red, int c_green, int c_blue)
{
    red = c_red;
    green = c_green;
    blue = c_blue;
}

void Color::set_color(const std::string& color_name)
{
    if (!color_name.compare("Black")) {red = 0; green = 0; blue = 0;} else
    if (!color_name.compare("White")) {red = 255; green = 255; blue = 255;} else
    if (!color_name.compare("Red")) {red = 255; green = 0; blue = 0;} else
    if (!color_name.compare("Lime")) {red = 0; green = 255; blue = 0;} else
    if (!color_name.compare("Blue")) {red = 0; green = 0; blue = 255;} else
    if (!color_name.compare("Yellow")) {red = 255; green = 255; blue = 0;} else
    if (!color_name.compare("Cyan")) {red = 0; green = 255; blue = 255;} else
    if (!color_name.compare("Aqua")) {red = 0; green = 255; blue = 255;} else
    if (!color_name.compare("Magenta")) {red = 255; green = 0; blue = 255;} else
    if (!color_name.compare("Fuchsia")) {red = 255; green = 0; blue = 255;} else
    if (!color_name.compare("Silver")) {red = 192; green = 192; blue = 192;} else
    if (!color_name.compare("Gray")) {red = 128; green = 128; blue = 128;} else
    if (!color_name.compare("Maroon")) {red = 128; green = 128; blue = 0;} else
    if (!color_name.compare("Olive")) {red = 128; green = 128; blue = 0;} else
    if (!color_name.compare("Green")) {red = 0; green = 128; blue = 0;} else
    if (!color_name.compare("Purple")) {red = 128; green = 0; blue = 128;} else
    if (!color_name.compare("Teal")) {red = 0; green = 128; blue = 128;} else
    if (!color_name.compare("Navy")) {red = 0; green = 0; blue = 128;}
}

int Color::get_red()
{
    return red;
}
int Color::get_green()
{
    return green;
}
int Color::get_blue()
{
    return blue;
}