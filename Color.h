//
// Created by n.chernyj on 27.11.2019.
//

#ifndef DRAWER_COLOR_H
#define DRAWER_COLOR_H

#include <string>

class Color{
    int red, green, blue;
public:
    void set_color(int red, int green, int blue);
    void set_color(const std::string& color_name);
    int get_red();
    int get_green();
    int get_blue();
    Color();
};


#endif //DRAWER_COLOR_H
