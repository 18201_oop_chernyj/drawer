//
// Created by admin on 11.12.2019.
//

#ifndef DRAWER_SHAPEFUNCTIONS_HPP
#define DRAWER_SHAPEFUNCTIONS_HPP

#include "Shape.h"

double totalArea() {
    return 0;
}

template<class Head, class... Tail>
double totalArea(const Head &head, const Tail &... tail) {
    return head.square() + totalArea(tail...);
}

void printNames(std::iostream &out_stream) {
    out_stream << '\n';
}

template<class Head, class... Tail>
void printNames(std::iostream &out_stream, const Head &head, const Tail &... tail) {
    out_stream << head.getName() + " ";
    printNames(out_stream, tail...);
}

void drawInput(std::istream &input, const std::string output_name) {
    std::string line;
    getline(input, line);
    bitmap_image img(stoi(line.substr(0, line.find('x'))),
                     stoi(line.substr(line.find('x') + 1, line.length())));
    img.set_all_channels(255, 255, 255);
    image_drawer drawer(img);
    drawer.pen_width(1);

    double arg0, arg1, arg2, arg3, coord_x, coord_y;
    int red, green, blue;
    while (getline(input, line)) {
        switch (line[0]) {
            case 'R': //Rectangle
            {
                line.erase(0, 10);
                if (line.find_first_of('(') != std::string::npos) {
                    arg0 = std::stof(line.substr(1, line.find_first_of(',')));
                    arg1 = std::stof(line.substr(line.find_first_of(',') + 1, line.find_first_of(')')));
                    line.erase(0, line.find_first_of(')') + 1);
                } else {
                    arg0 = 0;
                    arg1 = 0;
                }

                if (line.find_first_of('[') != std::string::npos) {
                    coord_x = std::stof(line.substr(line.find_first_of('[') + 1, line.find_first_of(',')));
                    coord_y = std::stof(line.substr(line.find_first_of(',') + 1, line.find_first_of(']')));
                    line.erase(0, line.find_first_of(']') + 1);
                } else {
                    coord_x = 0;
                    coord_y = 0;
                }
                Rectangle rectangle(coord_x, coord_y, arg0, arg1);
                if (line.find_first_of('{') != std::string::npos) {
                    if (line.find_first_of(',') != std::string::npos) {
                        red = std::stoi(line.substr(line.find_first_of('{') + 1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        green = std::stoi(line.substr(1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        blue = std::stoi(line.substr(1, line.find_first_of('}')));

                        rectangle.set_color(red, green, blue);
                    } else
                        rectangle.set_color(line.substr(line.find_first_of('{') + 1, line.find_first_of('}') - 2));
                }

                rectangle.draw(drawer);
            }
            case 'C': //Circle
            {
                line.erase(0, 7);
                if (line.find_first_of('(') != std::string::npos) {
                    arg0 = std::stof(line.substr(line.find_first_of('(') + 1, line.find_first_of(')')));
                    line.erase(0, line.find_first_of(')') + 1);
                } else {
                    arg0 = 0;
                }

                if (line.find_first_of('[') != std::string::npos) {
                    coord_x = std::stof(line.substr(line.find_first_of('[') + 1, line.find_first_of(',')));
                    coord_y = std::stof(line.substr(line.find_first_of(',') + 1, line.find_first_of(']')));
                    line.erase(0, line.find_first_of(']') + 1);
                } else {
                    coord_x = 0;
                    coord_y = 0;
                }
                Circle circle(coord_x, coord_y, arg0);
                if (line.find_first_of('{') != std::string::npos) {
                    if (line.find_first_of(',') != std::string::npos) {
                        red = std::stoi(line.substr(line.find_first_of('{') + 1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        green = std::stoi(line.substr(1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        blue = std::stoi(line.substr(1, line.find_first_of('}')));

                        circle.set_color(red, green, blue);
                    } else
                        circle.set_color(line.substr(line.find_first_of('{') + 1, line.find_first_of('}') - 2));
                }

                circle.draw(drawer);
            }
            case 'T': //Triangle
            {
                line.erase(0, 8);
                if (line.find_first_of('(') != std::string::npos) {
                    arg0 = std::stof(line.substr(line.find_first_of('(') + 1, line.find_first_of(',')));
                    line.erase(0, line.find_first_of(',') + 1);
                    arg1 = std::stof(line.substr(1, line.find_first_of(',')));
                    line.erase(0, line.find_first_of(',') + 1);
                    arg2 = std::stof(line.substr(1, line.find_first_of(',')));
                    line.erase(0, line.find_first_of(',') + 1);
                    arg3 = std::stof(line.substr(1, line.find_first_of(')')));
                } else {
                    arg0 = 0;
                    arg1 = 0;
                    arg2 = 0;
                    arg3 = 0;
                }

                if (line.find_first_of('[') != std::string::npos) {
                    coord_x = std::stof(line.substr(line.find_first_of('[') + 1, line.find_first_of(',')));
                    coord_y = std::stof(line.substr(line.find_first_of(',') + 1, line.find_first_of(']')));
                    line.erase(0, line.find_first_of(']') + 1);
                } else {
                    coord_x = 0;
                    coord_y = 0;
                }
                Triangle triangle(coord_x, coord_y, arg0, arg1, arg2, arg3);
                if (line.find_first_of('{') != std::string::npos) {
                    if (line.find_first_of(',') != std::string::npos) {
                        red = std::stoi(line.substr(line.find_first_of('{') + 1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        green = std::stoi(line.substr(1, line.find_first_of(',')));
                        line.erase(0, line.find_first_of(',') + 1);
                        blue = std::stoi(line.substr(1, line.find_first_of('}')));

                        triangle.set_color(red, green, blue);
                    } else
                        triangle.set_color(line.substr(line.find_first_of('{') + 1, line.find_first_of('}') - 2));
                }

                triangle.draw(drawer);
            }
        }
    }
    img.save_image(output_name);
}

#endif //DRAWER_SHAPEFUNCTIONS_HPP
