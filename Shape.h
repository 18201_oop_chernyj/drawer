//
// Created by n.chernyj on 20.11.2019.
//

#ifndef OOP_TRITSET_SHAPE_H
#define OOP_TRITSET_SHAPE_H

#include "Color.h"
#include "Named.h"

#include <string>
#include <cmath>
#include <cstdarg>
#include <utility>
#include <map>
#include <vector>
#include <iostream>
#include "bitmap/bitmap_image.hpp"

#define PI 3.14
//namespace shape {
    typedef struct st_segment {
        std::pair<double, double> point_1, point_2;
    } segment;

    class Shape : public Named, public Color {
    public:
        std::pair<double, double> coordinates;

        void draw(image_drawer &drawer);

        virtual double square() const = 0;

        virtual std::vector<segment> getSegments() const = 0;

        Shape() {};
    };

    class Rectangle : virtual public Shape {
        std::pair<double, double> dimensions;

    public:
        Rectangle(std::pair<double, double> center, std::pair<double, double> dimensions);

        Rectangle(std::pair<double, double> dimensions);
        Rectangle(double center_x, double center_y, double dimensions_x, double dimensions_y);
        Rectangle(double dimensions_x, double dimensions_y);

        virtual double square() const;

        virtual std::vector<segment> getSegments() const;
    };

    class Circle : virtual public Shape {
        double radius;

    public:
        Circle(std::pair<double, double> center, double rad);
        Circle(double center_x, double center_y, double rad);
        Circle(double rad);

        virtual double square() const;

        virtual std::vector<segment> getSegments() const;
    };

    class Triangle : virtual public Shape {
        std::pair<double, double> point_1, point_2;

    public:
        Triangle(std::pair<double, double> coord_0, std::pair<double, double> coord_1,
                 std::pair<double, double> coord_2);
        Triangle(double point0_x, double point0_y, double point1_x, double point1_y, double point2_x, double point2_y);
        Triangle(std::pair<double, double> coord_1, std::pair<double, double> coord_2);

        virtual double square() const;

        virtual std::vector<segment> getSegments() const;
    };
//}


#endif //OOP_TRITSET_SHAPE_H
